# Scripts de instalação - LaSE

Os scripts contidos neste repositório têm por objetivo facilitar e uniformizar a instalação de ferramentas nos computadores do Laboratório de Sistemas Embarcados, instalado na sala S10 do LABOTEC III, na UEFS.

Foram criados pensando em uma distribuição Debian, e leva em consideração as ferramentas que o ramo IEEE-RAS utiliza para executar as tarefas mais comuns aos seus projetos.

## Dependencias

Os scripts assumem que a instalação da distribuição ```Debian``` já vêm com servidor ```SSH```(SCP também), ```Avahi Daemon``` (para resolução de nomes na rede), e que os repositórios APT também foram instalados com marcadores ```contrib non-free```.

* SSHd
* SCP
* Avahi Daemon
* Sources.list

### Ferramentas

Além das dependencias instaladas com o SO, é necessário baixar os instaladores e executáveis das ferramentas utilizadas no ramo na pasta ```$HOME/apps/inst```:
1. [Arduino 1.8.19](https://downloads.arduino.cc/arduino-1.8.19-linux64.tar.xz "arduino-1.8.19-linux64.tar.xz") - arduino18.tar.zx
2. [VS Code](https://code.visualstudio.com/download ".deb x64") - vscode.deb
3. [Drivers para as placas WAVGAT UNO](https://www.pcboard.ca/downloads/wavgat-uno-2019-05-10.zip "wavgat-uno-2019-05-10.zip") - wavgat-drivers.zip

**ATENÇÂO** - Os arquivos devem ser salvos com os nomes **indicados acima**, na pasta ```$HOME/apps/inst/```, para que o script funcione corretamente!!

## Scripts Principais

Os scripts mais importantes são:

1. ```push_batch.sh``` - script para espaçhar os arquivos necessários e instaladores para os computadores na rede ( executado no host ).
2. ```install_tools.sh``` - script principal que executa as configurações e instalações das ferramentas ( executado no remoto ).
3. ```list``` - arquivo com os nomes dos computadores na rede para onde os arquivos serão copiados ( arquivo consumido pelo sccript ```push_batch.sh```).

## Uso

Os scripts são executados tanto no computador ```host``` (computador onde o operador está) e no computador ```remoto```(computador onde se quer instalar as ferramentas via ```ssh```)

Para garantir a boa execução dos scripts, é necessário estar na pasta onde os scripts estão salvos:

```bash

# Estando a pasta no $HOME
$ cd ~/scripts

```

### 1. list

O primeiro script a ser executado é o ```push_batch.sh```, e ele precisa dos nomes das máquinas para onde se quer copiar os arquivos. Os nomes deve ser colocados no arquivo ```list```, cada nome de computador em uma linha, seguindo o padrão: ```<nome_da_maquina>.local``` (padrão e endereço de rede fornecido pelo **Avahi**).

### 2. push_batch.sh

#### 2.1 Credenciais SSH

Para facilitar a comunicação com os computadores remotos é recomendado estabelecer a troca de chaves de autenticação SSH com o computador host, para tanto, o script ```spread_keys.sh``` realiza esse processo aproveitando a lista de computadores em ```list```.

```bash

# A partir da pasta $HOME/scripts/ por exemplo
$ ./spread_keys.sh

```

#### 2.2 Cópia dos arquivos em lote

Para executar o ```push_batch.sh``` verificar se os nomes dos computadores estão configurados no arquivo ```list``` e que os arquivos de instalação estão baixados e com os nomes corretos ns pasta ```$HOME/apps/inst/```, alem de executar os scripts a partit da pasta onde eles foram clonados do repositório.

```bash

# A partir da pasta $HOME/scripts/ por exemplo
$ ./push_batch.sh

```

### 3. install_tools.sh

O script ```install_tools.sh``` deve ser executado em uma sessão remota via **SSH**, permitindo a instalação das ferramentas em cada computador de forma quase paralela.

O script instala as ferramentas baixadas na pasta ```$HOME/apps/inst/```, instala também algumas ferramentas a partir dos repositórios Debian, configura o git na máquina remota para os usuários do RAS-UEFS e também as permissões dos usuários para uso das portas USB.

* Para o funcionamento correto é necessário conferir no script os nomes de usuários administrador e usuário aluno

```bash

# Do computador host locando com usuário adm na pasta scripts no remoto
$ ssh lase@lase-xxxx.local:~/scripts/

# Após autenticação no remoto
$ ./install_tools.sh

```

**DICA**: Será necessário abrir várias sessões (janelas do terminal) para executar essa tarefa de forma paralela.


## Observações

O script ```push_batch.sh``` fará a cópia dos arquivos via ```scp```, portanto se é a primeira vez que cada computador está sendo acessado via ```ssh``` as formas de autenticação inicial seráo exigidas. Pode ser mais interessante testar a conexão via ```ssh``` com cada máquina antes para diminuir os problemas de autenticação, e se ainda quiser melhorar mais ainda esse processo, é **FORTEMENTE RECOMENDADO** usar autenticação ssh por troca de chave, configurando a autenticação com uma chave padrão. Acesse [ssh_copy_id](https://www.ssh.com/academy/ssh/copy-id) e [ssh login sem senha](https://linuxize.com/post/how-to-setup-passwordless-ssh-login/) para mais informações.