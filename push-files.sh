#!/bin/bash

./logo.sh

MAQBASE="lase-0547"
MAQUINAS=(
    "${MAQBASE}71.local"
    "${MAQBASE}72.local"
    "${MAQBASE}73.local"
    "${MAQBASE}74.local"
    "${MAQBASE}75.local"
    "${MAQBASE}81.local"
    "${MAQBASE}82.local"
    "${MAQBASE}83.local"
)

for MAQ in "${MAQUINAS[@]}"; do
    echo "$MAQ"
    ssh $MAQ 'mkdir -p $HOME/apps/inst'
    scp $HOME/apps/init/* $MAQ:~/apps/inst/
done