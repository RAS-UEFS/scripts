#!/bin/bash

./logo.sh

echo "Gerando chaves de acesso..."
ssh-keygen

echo "Copiando chaves para os remotos..."
while read -r MAQ; do
    echo "$MAQ"
    ssh-copy-id $MAQ
done <list
