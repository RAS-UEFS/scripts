#!/usr/bin/env bash
./logo.sh

C1="\e[32m"
C2="\e[31m"
R="\e[0m"

FILESDIR=$HOME/apps/inst
ARDUINODIR=$HOME/apps/arduino-1.8.19
VSCODE=vscode.deb
ARDUINO=arduino18.tar.xz
WAVGAT=wavgat-drivers.zip

USERALUNO=aluno-lase

SUCCESS=1

echo -e "${C1}Instalador de Ferramentas RAS-UEFS$R"
echo -e "${C1}Instalando dependencias...$R"
sudo apt install python3-distutils python3-venv git htop aptitude freecad unzip
if [[ $? -ne 0 ]]; then
    SUCCESS=0
    exit;
fi

echo -e "${C1}  Configurando git...$R"
git config --global init.defaultBranch main
git config --global user.name "RAS-UEFS"
git config --global user.email "rasuefs@gmail.com"
if [[ $? -ne 0 ]]; then
    SUCCESS=0
fi

echo -e "${C1}Instalando VSCode...$R"
echo -e "  Verificando arduivos...$R"
if [[ -e $FILESDIR/$VSCODE ]]; then
    sudo apt install $FILESDIR/$VSCODE
    code --install-extension platformio.platformio-ide
    if [[ $? -ne 0 ]]; then
        SUCCESS=0
    fi
else
    echo -e "${C2}  Arquivos não encontrados [$VSCODE]$R"
    SUCCESS=0
fi

echo -e "${C1}Instalando Arduino-IDE...$R"
echo -e "  Verificando arduivos...$R"
if [[ -e $FILESDIR/$ARDUINO ]]; then
    tar -xf $FILESDIR/$ARDUINO -C $HOME/apps/
    sudo $ARDUINODIR/install.sh
else
    echo -e "${C2}  Arquivos não encontrados [$ARDUINO]$R"
    SUCCESS=0
fi

echo -e "${C1}Instalando placas para o arduino...$R"
echo -e "  Verificando arduivos...$R"
if [[ -e $FILESDIR/$WAVGAT ]]; then
    unzip $FILESDIR/$WAVGAT -d $FILESDIR
    cp -r $FILESDIR/update/* $ARDUINODIR
    echo -e "${C1} Configuração de permissões de usuários...$R"
    sudo adduser $USER dialout
    sudo adduser $USERALUNO dialout
else
    echo -e "${C2}  Arquivos não encontrados [$WAVGAT]$R"
    SUCCESS=0
fi

if [[ $SUCCESS -eq 1 ]]; then
    echo
    echo -e "${C1}==================================================$R"
    echo
    echo -e "${C1}!!INSTALAÇÂO CONCLUÍDA COM SUCESSO!!$R"
    echo
    echo
else
    echo
    echo -e "${C2}==================================================$R"
    echo
    echo -e "${C2}!!FALHA NA INSTALAÇÃO!!$R"
    echo
    echo
fi
