#!/bin/bash

./logo.sh

while read -r MAQ; do
    echo "$MAQ"
    ssh $MAQ 'mkdir -p $HOME/scripts'
    scp ./* $MAQ:~/scripts/
    ssh $MAQ 'mkdir -p $HOME/apps/inst'
    scp $HOME/apps/inst/* $MAQ:~/apps/inst/
done <list